
## Create the project 

```bash
subo create project tada
# build
cd tada
subo build .

```

## Add a new function (🤚 == runnable)

```bash
cd tada
#subo create runnable hey
subo create runnable hey --lang swift
subo build .
```



## Start Atmo
```bash
docker run -v /workspace/play-with-atmo/tada:/home/atmo -e ATMO_HTTP_PORT=8080 -p 8080:8080 suborbital/atmo:latest atmo --wait

curl localhost:8080/hello -d 'from Jane'
curl localhost:8080/hey -d 'from John'
```
