# play-with-atmo

> https://github.com/suborbital/atmo

```bash
subo create project important-api
cd important-api
subo build .

```

```bash
cd important-api
subo dev

curl localhost:8080/hello -d 'from the Kármán line!'
```

```bash
docker run -v /workspace/play-with-atmo/important-api:/home/atmo -e ATMO_HTTP_PORT=8080 -p 8080:8080 suborbital/atmo:latest atmo --wait

curl localhost:8080/hello -d 'from the Kármán line!'
```

## Add a new function (🤚 == runnable)

```bash
cd important-api
#subo create runnable hey
subo create runnable heypeolpe --lang swift
subo build .
```

gp url 8080
curl https://8080-tan-crow-njsm8qye.ws-eu17.gitpod.io/hey -d 'Bob Morane'